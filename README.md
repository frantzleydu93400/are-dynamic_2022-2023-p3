# ARE G6 - *insérer titre*

Par Anatole SAINERO, Thomas VEY, Aloïs MERELLE et Frantzley DURAND,
élèves de Sorbonne Université, en L1 portail Sciences Formelles.

## Description

Ce projet vise à répondre à la problématique suivante :
> *insérer problématique*

Cela passera par la modélisation et la simulation d'agents pouvant décider de leur moyen de transport quotidien, en parallèle de sous-modèles permettant d'associer le nombre d'usager à l'efficacité d'un certain moyen de transport.

## Site web

The website (empty for now) is available at <https://are-dynamic-2022-2023-g6.gitlab.io/are-dynamic_2022-2023-p3/>
