class TransportModel:

    def __init__(self, capacity, slowdown_threshold=0, slowdown_rate=1):
        """"Capacity : nb maximum d'utilisateurs (en nb d'agents ou en nb d'utilisateurs), 
        slowdown_threshold : une limite (en nb d'utilisateurs) à partir de laquelle la vitesse ralentis,
        slowdown_rate : la "vitesse" à laquelle la vitesse ralentis."""

        self.capacity = capacity
        self.slowdown_threshold = slowdown_threshold
        self.slowdown_rate = slowdown_rate
        self.history = []

    def update_history(self, n_users=0):
        """Au DEBUT de chaque journée, utiliser cette fonction avec le nombre d'utilisateurs qui ont l'intention d'utiliser ce moyen de transport."""
        self.history.append(n_users)

    def get_last_history(self, nb_jours=1):
        """Renvoie l'historique des nb_jours derniers jours, sous forme de (numero_jour, vitesse_jour)"""
        res = []
        for i in range(max(len(self.history)-nb_jours, 0), len(self.history)+1, 1):
            res.append((i, calcul_speed(i)))
        return res

    def remove_history(self, n=0):
        """Enleve le jour n de l'historique, pas forcément utile."""
        if self.history:
            self.history.pop(n)

    def calcul_speed(self, n):
        """Renvoie la vitesse (entre 0 et 1) du jour n, pour l'instant linéairement ensuite avec les diagrammes fondamentaux et tout et tout -> ça va être long"""
        n_users = self.history[n] if (len(self.history) >= n+1) else 0

        if n_users <= 0:
            return 1.0
        elif n_users >= self.capacity:
            return 0.0
        elif n_users >= self.slowdown_threshold:
            slowdown_ratio = (n_users - self.slowdown_threshold) / \
                (self.capacity - self.slowdown_threshold)
            return 1.0 - slowdown_ratio * self.slowdown_rate
        else:
            return 1.0
