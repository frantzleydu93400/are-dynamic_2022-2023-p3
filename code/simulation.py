from graphics import Visualiser, smart_calculation
from agents import one_agent_presentation


def c(g, plot=0):
    parameters = g.get_parameters()
    smart_calculation(lambda: one_agent_presentation(
        g, parameters["metro"], parameters["voiture"], parameters["bus"], plot))


g = Visualiser([lambda: c(g, 0), lambda: c(g, 1), lambda: c(g, 2)], {
               "metro": float, "voiture": float, "bus": float})
g.mainloop()
