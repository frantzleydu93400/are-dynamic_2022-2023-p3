import tkinter as tk
from tkinter import filedialog, messagebox, ttk
from ttkthemes import ThemedTk
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
import matplotlib.pyplot as plt
import numpy as np
import os
import threading
from datetime import datetime


class Visualiser(ThemedTk):
    """A Tk window that can be used with the simulation program to visualize differents plots and modify parameters."""

    def __init__(self, launch_functions, parameters):
        """Creates the window and all the widgets within it."""
        ThemedTk.__init__(self, theme="clam")
        self.title("Data Visualiser")
        self.geometry("500x500+500+250")

        def on_closing():
            if threading.active_count() > 1:
                answer = messagebox.askokcancel(
                    "EXITING", "A calculation is running, do you want to stop it and quit ?", icon=messagebox.WARNING)
                if answer:
                    self.destroy()
            else:
                self.destroy()
        self.protocol("WM_DELETE_WINDOW", on_closing)

        frame = ttk.Frame(self)
        self.fig = Figure()
        ax = self.fig.add_subplot(111)
        self.canvas = FigureCanvasTkAgg(self.fig, master=self)
        plot_toolbar = NavigationToolbar2Tk(
            self.canvas, self, pack_toolbar=False)
        plot_toolbar.update()
        self.canvas.draw_idle()
        self.down_frame = ttk.Frame(self, padding=(5, 5))

        self.progressbar = ttk.Progressbar(
            self.down_frame, orient="horizontal", mode="determinate")
        self.progressbar.pack(side="bottom", fill="x")
        sep = ttk.Separator(self.down_frame, orient="horizontal")
        sep.pack(side="bottom", fill="x", pady=5)

        self.launch_frame = ttk.Frame(self.down_frame, padding=(
            5, 5), relief="solid", borderwidth=0.5)
        self.launch_frame.pack(side="left", fill="both")

        options_frame = ttk.Frame(self.down_frame, padding=(
            5, 5), relief="solid", borderwidth=0.5)
        options_frame.pack(side="left", fill="both", padx=2)

        stats_frame = ttk.Frame(self.down_frame, padding=(
            5, 5), relief="solid", borderwidth=0.5)
        stats_frame.pack(side="right", fill="both", expand=True)

        # La liste de TOUTES les variables modifiables par l'interface graphique, sous forme de tk.Var() ici mais sous forme "normale" après Visualiser.get_parameters().
        self.variables = {}
        for (k, v) in parameters.items():
            if v == bool:
                self.variables[k] = tk.BooleanVar()
            elif v == float:
                self.variables[k] = tk.DoubleVar(value=1.0)
            else:
                # TODO : si besoin d'autres types que Bool (valeurs etc), ajouter les autres types.
                messagebox.showwarning(
                    "PARAMETRES", "Ce type de paramètres n'est pas pris en charge.\n"+str(k))

        # Pour ajouter des boutons ou autre widgets, c'est ici
        # en suivant le modèle suivant :
        # button = ttk.Button(<là où ça va>, text = "text", command = dosomething)
        # button.pack()

        for (i, f) in enumerate(launch_functions):
            b = ttk.Button(self.launch_frame, text="Plot "+str(i),
                           command=lambda f=f: smart_calculation(f))
            b.pack(side="top")

        for k in self.variables:
            if parameters[k] == bool:
                checkbox = ttk.Checkbutton(
                    options_frame, text=k, variable=self.variables[k], onvalue=True, offvalue=False)
                checkbox.pack(side="top")
            elif parameters[k] == float:
                f = ttk.Frame(options_frame)
                l = ttk.Label(f, text=k)
                l.pack(side="left")
                slider = ttk.Spinbox(f, from_=0.0, increment=0.05, to=1.0,
                                     textvariable=self.variables[k], justify="right", width=3)
                slider.pack(side="right")
                f.pack(side="top", fill="x")
            else:
                # TODO: ajouter des sliders ou autres moyen de choisir des valeurs.
                pass

        save_parameters_button = ttk.Button(
            options_frame, text="Save options", command=self.save_parameters)
        save_parameters_button.pack(side="bottom")

        self.stats_label = ttk.Label(stats_frame, justify="left")
        self.stats_label.bind("<Configure>", lambda e: self.stats_label.config(
            wraplength=self.stats_label.winfo_width()))
        self.stats_label.pack(side="left", fill="both", expand=True)

        # pas touche au reste thanks !
        self.down_frame.pack(side="bottom", expand=1, fill="both")
        plot_toolbar.pack(side="top", fill="x")
        self.canvas.get_tk_widget().pack(side="top", fill="both", expand=1)
        frame.pack()

        # self.mainloop()

    def plot(self, datax, multiple_datay, legend=None, color=None):
        """Draws one or multiple plots (with same x axis) on the principal canvas.
        Peut recevoir une ou plusieurs listes en y data et est normalement plutôt robuste face aux erreurs.
        Attention de bien fournir des listes de même longueur en x et y -> "ValueError : x and y have different shapes" """
        if threading.current_thread() == threading.main_thread() and threading.active_count() >= 2:
            for t in threading.enumerate():
                if t.name == "Calculation":
                    messagebox.showerror(
                        "PLOT", "Un lourd calcul est en cours, vous ne pouvez pas afficher autre chose pendant ce temps !")
                    return

        self.fig.clear()
        ax = self.fig.add_subplot(111)
        self.fig.subplots_adjust(left=0.1, bottom=0.095, right=0.96, top=0.93)

        # si datay est une liste de listes
        if all((isinstance(x, (list, np.ndarray, tuple))) for x in multiple_datay):
            for i in range(len(multiple_datay)):
                c = color
                if isinstance(color, (list, tuple, np.ndarray)):
                    if i < len(color):
                        c = color[i]
                    else:
                        c = None
                _ = ax.plot(datax, multiple_datay[i], color=c)

        # si datay contient des listes et des valeurs simples
        elif any((isinstance(x, (list, np.ndarray, tuple))) for x in multiple_datay):
            messagebox.showerror("Erreur", "Valeurs invalides pour plot")

        else:  # si datay ne contient que des valeurs simples <=> datay est la liste des données elle-même
            c = color
            if isinstance(color, (list, tuple, np.ndarray)):
                c = color[0]
            _ = ax.plot(datax, multiple_datay, color=color)

        if legend is not None:
            if isinstance(legend, str):  # si un seul string a été donné dans l'option legend
                ax.legend([legend], loc="best")
            else:  # si une liste de string a été donnée
                ax.legend(legend, loc="best")

        self.canvas.draw_idle()

    def print_into_gui(self, text):
        """Ecrit dans le label à droite."""
        self.stats_label["text"] = text
        self.stats_label.update()

    def update_progressbar(self, value=0, force=False):
        """Updates the Progressbar to the given value, between 0 and 100
        A utiliser pour afficher l'avancée des longues simulations, mais à ne pas utiliser massivement car ralentis les calculs."""
        if force:
            self.progressbar["value"] = 100
            self.progressbar.update()
            self.progressbar.update_idletasks()
            return
        if abs(self.progressbar["value"] - value) >= 1:
            self.progressbar["value"] = min(100, value)
            if value > 100:
                messagebox.showerror(
                    "Progressbar", "La valeur donnée à la barre de chargement est trop élevée !")

    def get_parameters(self):
        """Renvoie un dictionnaire contenant tous les paramètres"""
        res = {}
        for k, v in self.variables.items():
            res[k] = v.get()
        return res

    def save_parameters(self):
        """Sauvegarde les paramètres dans un fichier texte choisi"""
        data = [("texte", "*.txt"), ("all files", "*.*")]
        try:
            with filedialog.asksaveasfile(
                    mode="w", filetypes=data, defaultextension=".txt", initialdir=".", initialfile="options.txt") as f:
                f.write(datetime(2000, 1, 1).now().isoformat(
                    sep=' ', timespec="seconds"))
                for (k, v) in self.get_parameters().items():
                    f.write("\n" + str(k) + " : " + str(v))
        except Exception as e:
            print("Erreur :", e)
            messagebox.showerror(
                "FILE", "Une erreur a eu lieu lors de l'enregistrement du fichier.")
            return
        else:
            messagebox.showinfo("FILE", "Le fichier a bien été enregistré !")
        for (k, v) in self.get_parameters().items():
            print(k, v, type(v))

    def long_iter_wrapper(self, iter):
        """Wraps around an iterator to make the Progressbar update and a lot more quality of life stuff.
        A utiliser autour de l'iteration principale
        Utile pour les longs (pas instantanés) calculs principalement."""

        if not (hasattr(iter, '__iter__') or hasattr(iter, '__getitem__')):
            messagebox.showerror(
                "ITER", "The long iteration wrapper only works with range objects.")
            return

        for w in self.launch_frame.winfo_children():
            w.configure(state="disabled")

        self.fig.clear()
        self.fig.text(0.5, 0.5, "Calcul en cours...", color="red", fontsize=12,
                      horizontalalignment="center", verticalalignment="center")
        self.fig.text(0.5, 0.4, "Par mesure de sécurité, impossible de lancer d'autres calculs pendant ce temps.",
                      color="red", fontsize=8, horizontalalignment="center")
        self.fig.text(0.5, 0.3, "Pour l'arrêter prématurément (boucle infinie par exemple), fermez la fenêtre.",
                      color="red", fontsize=9, horizontalalignment="center")
        self.canvas.draw()
        self.update_progressbar(0, force=True)

        steps = iter.stop - iter.start
        count = 0

        for i in iter:
            count += 1
            self.update_progressbar(count*100/steps)
            yield i

        self.update_progressbar(100, force=True)
        for w in self.launch_frame.winfo_children():
            w.configure(state="normal")


def smart_calculation(function):
    """Launch the calculation in a thread so that the window stays responsive during it.
    A utiliser de préférence pour les longs calculs !"""
    threading.Thread(target=function, daemon=True, name="Calculation").start()

# v = Visualiser()
