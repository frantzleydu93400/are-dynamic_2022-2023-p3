import random
from graphics import Visualiser, smart_calculation
import numpy as np


# Variables globales
# preference_metro = 0.7
# preference_voiture = 0.2
# preference_bus = 0.1


class Agent: #TODO : documentation pls, ajout sub-models
    def __init__(self, voiture_pref, metro_pref, bus_pref):
        self.voiture_pref = voiture_pref
        self.metro_pref = metro_pref
        self.bus_pref = bus_pref
        self.voiture_pref_max = voiture_pref
        self.metro_pref_max = metro_pref
        self.bus_pref_max = bus_pref
        self.satisfaction = 1.0
        self.satisfaction_history = []

    def choose_mode_of_transport(self, voiture_retard, metro_retard, bus_retard):
        best_mode = None
        if voiture_retard <= min(metro_retard, bus_retard):
            best_mode = "voiture"
        elif metro_retard <= min(voiture_retard, bus_retard):
            best_mode = "metro"
        else:
            best_mode = "bus"
        return best_mode

    def update_preferences(self):
        self.satisfaction -= 0.1 * random.random()
        total = self.voiture_pref + self.metro_pref + self.bus_pref
        self.voiture_pref = max(0.0, self.voiture_pref + self.satisfaction *
                                (self.voiture_pref_max - self.voiture_pref / total))
        self.metro_pref = max(0.0, self.metro_pref + self.satisfaction *
                              (self.metro_pref_max - self.metro_pref / total))
        self.bus_pref = max(0.0, self.bus_pref + self.satisfaction *
                            (self.bus_pref_max - self.bus_pref / total))
        self.satisfaction_history.append(self.satisfaction)

    def satisfaction_ratio(self):
        return self.satisfaction

    def best_mode_and_preference(self):
        best_mode = None
        best_pref = max(self.voiture_pref, self.metro_pref, self.bus_pref)
        if best_pref == self.voiture_pref:
            best_mode = "voiture"
        elif best_pref == self.metro_pref:
            best_mode = "metro"
        else:
            best_mode = "bus"
        return best_pref, best_mode

    def assign_best_mode(self, best_mode):
        if best_mode == "voiture":
            self.voiture_pref += 0.1
        elif best_mode == "metro":
            self.metro_pref += 0.1
        else:
            self.bus_pref += 0.1


def one_agent_presentation(graphics: Visualiser, preference_voiture,  preference_metro, preference_bus, plot=0):

    agent = Agent(preference_voiture, preference_metro, preference_bus)

    N = 31
    iterator = graphics.long_iter_wrapper(range(N))
    best_pref_history = []
    best_mode_history = []
    for i in iterator:  # pour N jours
        # pour simuler tous les retard (peut etre changer) -> utiliser sous-modèles plutôt ? 
        voiture_retard = random.uniform(0, 30)
        metro_retard = random.uniform(0, 15)
        # le retard a 2 fois plus de retard que le bus (en test)
        bus_retard = random.uniform(0, 30) * 2
        best_mode = agent.choose_mode_of_transport(
            voiture_retard, metro_retard, bus_retard)
        print("\n ---------------------")
        print("Jour", i+1)
        print("voiture retard:", voiture_retard)
        print("Metro retard:", metro_retard)
        print("Bus retard:", bus_retard)
        print("Best mode:", best_mode)
        agent.update_preferences()
        print("voiture preference:", agent.voiture_pref)
        print("Metro preference:", agent.metro_pref)
        print("Bus preference:", agent.bus_pref)
        print("Satisfaction ratio:", agent.satisfaction_ratio())
        best_pref, best_mode = agent.best_mode_and_preference()
        best_pref_history.append(best_pref)
        best_mode_history.append(best_mode)
        print("Meilleur preference:", best_pref)
        print("Meilleur mode of transport:", best_mode)
        agent.assign_best_mode(best_mode)

    if plot == 0:
        graphics.plot(np.arange(N), agent.satisfaction_history, "satisfaction")
    elif plot == 1:
        graphics.plot(np.arange(N), best_pref_history, "best pref")
    elif plot == 2:
        r = []
        for i in best_mode_history:
            if i == "voiture":
                r.append(0)
            elif i == "metro":
                r.append(1)
            else:
                r.append(2)
        graphics.plot(np.arange(N), r, "best mode")
